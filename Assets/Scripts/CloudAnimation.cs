﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(SpriteRenderer))]
public class CloudAnimation : MonoBehaviour {

	const float SPEED = 0.21f;
	Transform pos;
	float check;

	float halfSize;
	void Start () {
		pos = GetComponent<Transform> ();
		halfSize = GetComponent<SpriteRenderer> ().bounds.extents.x;
	}
	
	// Update is called once per frame
	void Update () {
		check = Camera.main.ScreenToWorldPoint (Vector3.zero).x - halfSize;
		if (pos.position.x >= check) {
			pos.position -= new Vector3 (Time.deltaTime * SPEED, 0f, 0f);
		} else {
			Destroy (gameObject);
		}
	}
}
