﻿using UnityEngine;
using System.Collections;

public class ScrollObjects : MonoBehaviour
{

	public float speed = 5f;
	public bool axisY = true;
	public float check = 0f;
	private RectTransform RT;

	void Awake ()
	{
		RT = GetComponent<RectTransform> ();
	}

	void FixedUpdate ()
	{
		if (axisY) {
			if (RT.offsetMin.y != check) { 
				RT.offsetMin += new Vector2 (0f, speed);
				RT.offsetMax += new Vector2 (0f, speed);
			}
		} else {
			if (RT.offsetMin.x != check) { 
				RT.offsetMin += new Vector2 (speed, 0f);
				RT.offsetMax += new Vector2 (speed, 0f);
			}
		}
	}
}