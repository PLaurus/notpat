﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseSwitcher : SharedVariables {
	private bool pause = false;
	public GameObject pauseIcon;
	public GameObject playIcon;
	public Text pauseText;
	//public Joystick joystick;

	private void SwitchPause(){
		pauseText.gameObject.SetActive (false);
		pauseIcon.SetActive (true);
		playIcon.SetActive (false);
	}

	private void SwitchPlay(){
		playIcon.SetActive (true);
		pauseIcon.SetActive (false);
		pauseText.gameObject.SetActive (true);
	}

	public void SetPauseSceene(){
		if (pause) {
			curSceene = Sceene.GAME;
			pause = false;
			SwitchPause ();
		} else {
			curSceene = Sceene.PAUSE;
			pause = true;
			SwitchPlay ();
			//character.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;

		}
	}
}
