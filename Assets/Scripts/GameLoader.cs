﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameLoader : MonoBehaviour {

	//public GameObject loadingScreen;
	public Slider slider;
	public Text progressText;
	private const int SCENE_INDEX = 1;

	void Start () {
		StartCoroutine (LoadAsyncronously ());
	}

	/*public void LoadGame(int sceneIndex){
		
	}*/

	IEnumerator LoadAsyncronously(){
		AsyncOperation operation = SceneManager.LoadSceneAsync (SCENE_INDEX);

		//Loading

		while (!operation.isDone) {
			float progress = Mathf.Clamp01 (operation.progress / .9f);

			slider.value = progress;
			progressText.text = ((int)(progress*100)).ToString () + "%";
			yield return null;
		}
	}
}
