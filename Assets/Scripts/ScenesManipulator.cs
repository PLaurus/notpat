﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ScenesManipulator : SharedVariables
{
	public RectTransform scoreRectTransform,pauseRectTransform;
	public Transform startObjs;
	public Score score;
	public Text playText, gameName;
	public GameObject leftButtons, rightButtons;
	public TornadoMoving tornado;
	public CameraPosController cameraPosController;
	public GameObject looseText, highScoreGameObject;
	public GameObject highScoreContainer,SoundIconContainer;
	public Joystick joystick;
	public GameObject distanceObj;
	public GameObject bonusBox;
	public Bonus bonus;

	private Vector2 startObjsPos;

	void Start ()
	{
		startObjsPos = startObjs.position;
	}

	void ResetStartObjs(){
		startObjs.position = startObjsPos;
		startObjs.gameObject.SetActive (true);
	}

	void ScrollButtonsFromCenter(){
		foreach(Image image in leftButtons.GetComponentsInChildren<Image> ()) image.raycastTarget = false;
		leftButtons.GetComponent<ScrollObjects> ().speed = -10f;
		leftButtons.GetComponent<ScrollObjects> ().check = -200f;
		rightButtons.GetComponent<ScrollObjects> ().speed = 10f;
		rightButtons.GetComponent<ScrollObjects> ().check = 200f;
	}

	void ScrollButtonsToCenter(){
		foreach(Image image in leftButtons.GetComponentsInChildren<Image> ()) image.raycastTarget = true;
		leftButtons.GetComponent<ScrollObjects> ().speed = 10f;
		leftButtons.GetComponent<ScrollObjects> ().check = 0f;
		rightButtons.GetComponent<ScrollObjects> ().speed = -10f;
		rightButtons.GetComponent<ScrollObjects> ().check = 0f;
	}

	public void SceeneOperator ()
	{
		if (curSceene == Sceene.MENU) {
			curSceene = Sceene.GAME;
			bonusBox.SetActive (false);
			playText.gameObject.SetActive (false);
			scoreRectTransform.gameObject.SetActive (true);
			pauseRectTransform.gameObject.SetActive (true);

			distanceObj.SetActive (true);
			//map.SetActive (true);

			gameName.gameObject.SetActive (false);
			highScoreContainer.SetActive (false);
			SoundIconContainer.SetActive (false);
			ScrollButtonsFromCenter ();//good!
		} else if (curSceene == Sceene.RESULT_WINDOW) {
			curSceene = Sceene.MENU;
			if(bonus.todayCoinsBonus > 0){
				bonusBox.SetActive(true);
			}
			distanceObj.SetActive (false);
			//map.SetActive (false);

			looseText.SetActive (false);
			highScoreGameObject.SetActive (false);
			cameraPosController.resetPos ();
			gameName.gameObject.SetActive (true);
			playText.gameObject.SetActive (true);
			highScoreContainer.SetActive (true);
			SoundIconContainer.SetActive (true);
			scoreRectTransform.gameObject.SetActive (false);
			ScrollButtonsToCenter ();//good!
			ResetStartObjs ();//good!
			ResetCharacterPos ();//good!
			CheckPoint.ResetCheckPoints ();//good!
			obstaclesSpawner.Reset ();//good!
			score.Reset();//good!
			tornado.Reset ();//good!
		}
	}
}
