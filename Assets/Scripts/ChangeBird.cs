﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ChangeBird : SharedVariables {
	//#686868FF цвет неактивной птицы

	public Animator birdAnimator;

	public GameObject[] birdsGameObjects = new GameObject[8];
	public Sprite emptyCheckBox;
	public Sprite checkedCheckBox;

	public Text shopCoinsText;
	public Text gameCoinsText;
	public Coins coinsClass;

	public Saver saver;

	void Start () {
		//firstLaunch ();//временнож
		//ResetCheckBoxes ();

	}

	public void firstLaunch(){
		birds [0] = new Bird (0);
		birds [0].SetBought (true);
		birds [0].selected = true;
		birds [1] = new Bird (100);
		birds [2] = new Bird (500);
		birds [3] = new Bird (2000);
		birds [4] = new Bird (4000);
		birds [5] = new Bird (9000);
		birds [6] = new Bird (30000);
		birds [7] = new Bird (70000);
		ResetCheckBoxes ();
	}

	public void secondLaunch(){
		birds [0].SetCost (0);
		birds [1].SetCost (100);
		birds [2].SetCost (500);
		birds [3].SetCost (2000);
		birds [4].SetCost (4000);
		birds [5].SetCost (9000);
		birds [6].SetCost (30000);
		birds [7].SetCost (70000);
		ResetCheckBoxes ();
		for(int i=0;i<birds.Length;i++) {
			if (birds [i].selected)
				birdAnimator.SetInteger ("AnimationClip", i);
		}
	}

	void Update () {
		
	}

	public void ResetCoinsText(){
		shopCoinsText.text = coinsCount.ToString();
	}

	public void ChooseBird(int i){
		if (birds [i].IsBought ()) {
			birdAnimator.SetInteger ("AnimationClip", i);
			foreach (GameObject go in GameObject.FindGameObjectsWithTag("BirdCheckBox")) {
				go.GetComponent<Image>().sprite = emptyCheckBox;
			}
			foreach (Transform go in birdsGameObjects [i].GetComponentsInChildren<Transform>(true)) {
				if (go.gameObject.CompareTag ("BirdCheckBox"))
					go.gameObject.GetComponent<Image>().sprite = checkedCheckBox;
				foreach (Bird bird in birds)
					bird.selected = false;
				birds [i].selected = true;
				coinsClass.ChangeMultiplier ();
			}
		} else {
			if (coinsCount < birds [i].cost) { 
				Debug.Log("У вас недостаточно средств!");
			} else {
				coinsCount -= birds [i].cost;
				birds [i].SetBought (true);
				gameCoinsText.text = shopCoinsText.text = coinsCount.ToString();
				ResetBirdsBuyable();
				ResetCheckBoxes ();
				ResetCoinsText ();
			}

		}
		saver.Save ();
	}

	private void ResetCheckBoxes(){
		for(int i=0;i<birds.Length;i++){
			if (birds [i].IsBought ()) {
				foreach (Transform go in birdsGameObjects [i].GetComponentsInChildren<Transform>(true)) {
					if (go.gameObject.CompareTag ("BirdCost"))
						go.gameObject.SetActive (false);
					if (go.gameObject.CompareTag ("BirdCheckBox")) {
						if (birds [i].selected) {
							go.GetComponent<Image> ().sprite = checkedCheckBox;
							go.gameObject.SetActive (true);
						} else {
							go.GetComponent<Image> ().sprite = emptyCheckBox;
							go.gameObject.SetActive (true);
						}
					}
				}
			} else {
				foreach (Transform go in birdsGameObjects [i].GetComponentsInChildren<Transform>(true)) {
					if (go.gameObject.CompareTag ("BirdCost"))
						go.gameObject.SetActive (true);
					if (go.gameObject.CompareTag ("BirdCheckBox")) {
						go.gameObject.SetActive (false);
						go.GetComponent<Image> ().sprite = emptyCheckBox;
					}
				}
			}
		}
	}

	public void ResetBirdsBuyable(){
		for (int i = 0; i < birds.Length; i++) {
			if (birds [i].cost > coinsCount && !birds [i].IsBought()) {
				birdsGameObjects [i].GetComponent<Image> ().color = new Color (0.41f, 0.41f, 0.41f);
			}else birdsGameObjects [i].GetComponent<Image> ().color = new Color (1.0f, 1.0f, 1.0f);
		}
	}

	/*public void Buy(int i){
		birds [i].SetBought (true);
	}*/
}

[Serializable]
public class Bird{
	public uint cost { get; private set;}
	private bool isBought;
	public bool selected = false;
	public Bird(uint cost){
		this.cost = cost;
		isBought = false;
	}

	public void SetBought(bool isBought){
		this.isBought = isBought;
	}

	public bool IsBought(){
		return isBought;
	}

	public void SetCost(uint cost){
		this.cost = cost;
	}
}
