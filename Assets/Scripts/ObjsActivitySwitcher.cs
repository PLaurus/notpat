﻿using UnityEngine;
using System.Collections;

public class ObjsActivitySwitcher : SharedVariables {
	private const float sMultiplier = 2f;

	public bool startObject = false;
	//private ObstaclesSpawner obstaclesSpawner;

	private int platId, cloneId;
	private float screenLeftBorderPos;
	private float obstacleRightBorderPos;

	/*void Awake(){
		obstaclesSpawner = GameObject.FindGameObjectWithTag("GameEnvirorment").GetComponent<ObstaclesSpawner> ();
	}*/

	void Start () {
		//obstaclesSpawner = GameObject.FindGameObjectWithTag("GameEnvirorment").GetComponent<ObstaclesSpawner> ();
		setRightBorderPos ();
	}

	/*void OnEnable(){
		setRightBorderPos ();
	}*/

	void Update () {
		if (curSceene == Sceene.GAME) {
			screenLeftBorderPos = Camera.main.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x;
			if ((screenLeftBorderPos - obstacleRightBorderPos) > obstaclesSpawner.GetS () * sMultiplier) {
				if (startObject) {
					gameObject.SetActive (false);
				} else {
					transform.position = obstaclesSpawner.getDisabledObjsPos ();
					obstaclesSpawner.disablePlat (platId, cloneId);
				}
			}
		}
	}

	public void setIds(int platId,int cloneId){
		this.platId = platId;
		this.cloneId = cloneId;
	}

	public void setRightBorderPos(){
		obstacleRightBorderPos = GetComponent<SpriteRenderer>().transform.position.x +
			GetComponent<SpriteRenderer>().bounds.extents.x;
	}
}
