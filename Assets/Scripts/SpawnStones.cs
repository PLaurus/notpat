﻿using UnityEngine;
using System.Collections;

public class SpawnStones : MonoBehaviour {

	public GameObject stone;
	float spawnTime = 5f;
	const float ADDITIONAL_HEIGHT = 15f, MIN_SPAWN_TIME = 0f, MAX_SPAWN_TIME = 15f;
	void Start () {
		StartCoroutine (spawn ());
	}

	IEnumerator spawn(){
		while (true) {
			spawnTime = Random.Range (MIN_SPAWN_TIME, MAX_SPAWN_TIME);
			Vector3 pos = Camera.main.ScreenToWorldPoint (new Vector3 (Random.Range (0, Screen.width),
				(float) Screen.height + ADDITIONAL_HEIGHT, GameObject.FindGameObjectWithTag("Background").transform.position.z /2));
			Instantiate (stone,pos,Quaternion.Euler(0f, 0f, Random.Range (0f,360f)));
			yield return new WaitForSeconds (spawnTime);
		}
	}

	public static float GetAdditionalHeight(){
		return ADDITIONAL_HEIGHT;
	}
}