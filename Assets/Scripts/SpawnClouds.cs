﻿using UnityEngine;
using System.Collections;

public class SpawnClouds : MonoBehaviour {

	public GameObject[] clouds = new GameObject[4];
	public Transform parentTransform;
	private float spawnTime = 1f;
	const float MIN_SPAWN_TIME = 10f, MAX_SPAWN_TIME = 15f;

	void Start () {
		StartCoroutine (spawn ());
	}

	IEnumerator spawn(){
		while (true) {
			spawnTime = Random.Range (MIN_SPAWN_TIME, MAX_SPAWN_TIME);
			//Vector3 pos = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width + ADDITIONAL_WIDTH,
				//Random.Range(Screen.height*2/3,Screen.height*26/27), GameObject.FindGameObjectWithTag("Background").transform.position.z /2));
			int i = Random.Range (0, clouds.Length);

			Vector3 pos = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width ,
				Random.Range(Screen.height*2/3,Screen.height*26/27), GameObject.FindGameObjectWithTag("Background").transform.position.z /2));
			pos += new Vector3 (clouds [i].GetComponent<SpriteRenderer> ().bounds.extents.x, 0f, 0f);

			Instantiate (clouds[i],pos,Quaternion.identity,parentTransform);
			yield return new WaitForSeconds (spawnTime);
		}
	}

	/*public static float GetAdditionalWidth(){
		return ADDITIONAL_WIDTH;
	}*/
}
