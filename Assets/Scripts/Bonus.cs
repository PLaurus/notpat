﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Bonus : SharedVariables {

	public GameObject bonusBox;
	public Text bonusCoinsText;
	public Text coinsText;
	public Saver saver;

	private const int mondayBonus = 50;
	private const int tuesdayBonus = 55;
	private const int wednesdayBonus = 60;
	private const int thursdayBonus = 70;
	private const int fridayBonus = 75;
	private const int saturdayBonus = 100;
	private const int sundayBonus = 110;
	private const int range = 10;

	public DateTime previousDay { get; private set;}
	public uint todayCoinsBonus { get; private set;}

	private DateTime currentDay;

	void Start () {
		//previousDay = new DateTime (2017, 08, 17);
		currentDay = DateTime.Today;
		if (currentDay > previousDay) {
			todayCoinsBonus = 0;
			if(currentDay.DayOfWeek == DayOfWeek.Monday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (mondayBonus - range, mondayBonus + range);
			}else if(currentDay.DayOfWeek == DayOfWeek.Tuesday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (tuesdayBonus - range, tuesdayBonus + range);
			}else if(currentDay.DayOfWeek == DayOfWeek.Wednesday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (wednesdayBonus - range, wednesdayBonus + range);
			}else if(currentDay.DayOfWeek == DayOfWeek.Thursday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (thursdayBonus - range, thursdayBonus + range);
			}else if(currentDay.DayOfWeek == DayOfWeek.Friday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (fridayBonus - range, fridayBonus + range);
			}else if(currentDay.DayOfWeek == DayOfWeek.Saturday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (saturdayBonus - range, saturdayBonus + range);
			}else if(currentDay.DayOfWeek == DayOfWeek.Sunday){
				todayCoinsBonus += (uint)UnityEngine.Random.Range (sundayBonus - range, sundayBonus + range);
			}
			Debug.Log ("Сегодняшний бонус("+currentDay.DayOfWeek+"): " + todayCoinsBonus);
			previousDay = currentDay;
			saver.Save ();
		} else {
			Debug.Log ("Бонус уже сгенерирован!");
		}
		if (todayCoinsBonus > 0) {
			bonusCoinsText.text = todayCoinsBonus.ToString ();
			bonusBox.SetActive (true);
		}
	}

	public void obtainBonus(){
		coinsCount += todayCoinsBonus;
		coinsText.text = coinsCount.ToString();
		todayCoinsBonus = 0;
		bonusBox.SetActive (false);
		saver.Save ();
	}

	public void setPreviousDay(DateTime data){
		previousDay = data;
	}

	public void setCoinsBonus(uint newCoinsBonus){
		todayCoinsBonus = newCoinsBonus;
	}
	// Update is called once per frame
	void Update () {
	
	}

	//public First
}
