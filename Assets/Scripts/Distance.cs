﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Distance : SharedVariables {
	//public
	public SpriteRenderer tornado;
	public Text distanceText;
	public GameObject distanceObj;

	//private
	private float camLeftBorderPos = 0f;

	// Use this for initialization
	void Start () {
		camLeftBorderPos = Camera.main.ScreenToWorldPoint (Vector3.zero).x;
	}
	
	// Update is called once per frame
	void Update () {
		if (curSceene == Sceene.GAME) {
			camLeftBorderPos = Camera.main.ScreenToWorldPoint (Vector3.zero).x;
			float dist = camLeftBorderPos - tornado.transform.position.x - tornado.bounds.extents.x;
			if (dist > 0) {
				if (!distanceObj.activeInHierarchy)
					distanceObj.SetActive (true);
				distanceText.text = Math.Round (dist, 2).ToString ();
			} else if (distanceObj.activeInHierarchy) {
				distanceObj.SetActive (false);
			}
		}
	}
}
