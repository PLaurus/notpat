﻿using UnityEngine;
using System.Collections;

public class StoneAnimation : MonoBehaviour {

	const float SPEED = 5f;
	Transform pos;
	float check;
	void Start () {
		pos = GetComponent<Transform> ();
		check = Camera.main.ScreenToWorldPoint (new Vector3 (0f, -SpawnStones.GetAdditionalHeight(), 0f)).y;
	}

	void Update () {
		if (pos.position.y >= check) {
			pos.position -= new Vector3 (0f, Time.deltaTime * SPEED, 0f);
		} else {
			Destroy (gameObject);
		}
	}
}
