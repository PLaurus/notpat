﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//[RequireComponent(typeof(Slider))]
public class Map : SharedVariables {
	public SpriteRenderer tornado;
	public RectTransform miniTornado;
	//private Slider slider;
	private float maxDistation = 50.0f;
	void Start () {
		//slider = GetComponent<Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
		float dist = character.position.x - tornado.transform.position.x - tornado.bounds.extents.x;
		if (dist < maxDistation) {
			float value = dist / maxDistation;
			if (value >= 0f && value <= 1.0f) {
				miniTornado.anchorMax = new Vector2 (1.0f - value, 1.0f);
				miniTornado.anchorMin = new Vector2 (1.0f - value, 0f);
			}
		}
	}
}
