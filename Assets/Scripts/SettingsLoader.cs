﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SettingsLoader : SharedVariables {
	public GameObject SettingsCanvas;
	public GameObject[] gameObjsToDisable = new GameObject[3];
	public Achivements achievements;
	public Text coinsText;
	public ChangeBird changeBird;

	void Start () {
	
	}

	public void LoadSettingsWindow () {
		changeBird.ResetCoinsText ();
		changeBird.ResetBirdsBuyable ();
		coinsText.text = coinsCount.ToString ();
		SettingsCanvas.SetActive(true);
		curSceene = Sceene.SETTINGS;
		foreach (GameObject go in gameObjsToDisable) go.SetActive (false);
		achievements.IncrementAchievement (achievements.achievement8);
	}

	public void CloseSettingsWindow () {
		foreach (GameObject go in gameObjsToDisable) go.SetActive (true);
		curSceene = Sceene.MENU;
		SettingsCanvas.SetActive(false);
	}
}
