﻿using UnityEngine;
using System.Collections;

public class ButtonAnimation : SharedVariables {
	public AudioSource audioSource;
	private Vector3 startScale;
	void Start () {
		//startScale = transform.localScale;
	}

	public void PointerDown(){
		transform.localScale = new Vector3 (1.1f, 1.1f, 1.1f);
		audioSource.Play ();
	}

	public void PointerUp(){
		transform.localScale = new Vector3 (1f, 1f, 1f);
		//transform.localScale = startScale;
	}


}
