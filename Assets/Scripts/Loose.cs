﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[RequireComponent(typeof(Collider2D))]
public class Loose : SharedVariables
{
	//public
	public SpriteRenderer tornadoSpriteRenderer;
	public RectTransform pauseRectTransform;
	public GameObject looseText;
	public GameObject highScoreGameObject;
	public Achivements achievements;
	public Saver saver;
	public Text highScoreText,highScoreTextInfo;
	public AdBanner adBanner;
	//public CharacterMove characterMove;

	//private
	const uint achievementScore1 = 100;
	const uint achievementScore2 = 1000;
	const uint achievementScore3 = 5000;
	const uint achievementScore4 = 333;
	const uint achievementScore5 = 1001;
	Vector2 deathPosition;
	Text scoreText;
	const float RADIUS = 0.5f;

	void Start ()
	{
		deathPosition = new Vector2(0f,0f);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			GameOver ();
		}
	}

	void MoveCharacterToTornado(){
		deathPosition.x = tornadoSpriteRenderer.transform.position.x; //+ tornadoSpriteRenderer.bounds.extents.x;
		deathPosition.y = tornadoSpriteRenderer.transform.position.y;
		character.position = new Vector3 (deathPosition.x, deathPosition.y);
		character.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		//characterMove.
	}

	void GameOver ()
	{
		if (curSceene == Sceene.GAME){
			MoveCharacterToTornado ();
			pauseRectTransform.gameObject.SetActive (false);
			//if(character.position.x <= deathPosition) {
			curSceene = Sceene.RESULT_WINDOW;
			if (curScore >= achievementScore1) {
				achievements.UnlockAchivement (achievements.achievement1);
			}
			if (curScore >= achievementScore2) {
				achievements.UnlockAchivement (achievements.achievement2);
			}
			if (curScore >= achievementScore3) {
				achievements.UnlockAchivement (achievements.achievement3);
			}
			if (curScore == achievementScore4) {
				achievements.UnlockAchivement (achievements.achievement4);
			}else if(curScore == achievementScore5) {
				achievements.UnlockAchivement (achievements.achievement5);
			}
				
			if (curScore > highScore) {
				highScore = curScore;
				highScoreText.text = highScoreTextInfo.text = highScore.ToString ();
				highScoreGameObject.GetComponent<Text> ().text = "New High Score!\n" + highScore;
				highScoreGameObject.SetActive (true);
			} else {
				looseText.SetActive (true);
			}
			saver.Save ();

			achievements.IncrementAchievement (achievements.achievement11,Convert.ToInt32(curScore));
			achievements.ReportScore (Convert.ToInt32(curScore));
			adBanner.ShowVideoBanner();// реклама во все ебало
			//}
		}
	}
}
