﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndlessHills : MonoBehaviour {
	private SpriteRenderer currentHill1;
	private SpriteRenderer currentHill2;
	public SpriteRenderer[] hills1 = new SpriteRenderer[2];
	public SpriteRenderer[] hills2 = new SpriteRenderer[2];

	void Start () {
		SpriteRenderer temp1 = hills1 [0];
		SpriteRenderer temp2 = hills2 [0];
		currentHill1 = hills1 [1];
		currentHill2 = hills2 [1];
		currentHill1.transform.position = new Vector3(temp1.transform.position.x + temp1.bounds.extents.x +
			temp1.bounds.extents.x,temp1.transform.position.y,0f);
		currentHill2.transform.position = new Vector3(temp2.transform.position.x + temp2.bounds.extents.x +
			temp2.bounds.extents.x,temp2.transform.position.y,0f);
		currentHill1.gameObject.SetActive (true);
		currentHill2.gameObject.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (currentHill1.transform.position.x < Camera.main.ScreenToWorldPoint (Vector3.zero).x) {
			SpriteRenderer temp = currentHill1;
			currentHill1 = (currentHill1 == hills1 [0]) ? hills1 [1] : hills1 [0];
			currentHill1.transform.position = new Vector3(temp.transform.position.x + temp.bounds.extents.x +
				temp.bounds.extents.x,temp.transform.position.y,0f);
		}
		if (currentHill2.transform.position.x < Camera.main.ScreenToWorldPoint (Vector3.zero).x) {
			SpriteRenderer temp = currentHill2;
			currentHill2 = (currentHill2 == hills1 [0]) ? hills1 [1] : hills1 [0];
			currentHill2.transform.position = new Vector3(temp.transform.position.x + temp.bounds.extents.x +
				temp.bounds.extents.x,temp.transform.position.y,0f);
		}
	}
}
