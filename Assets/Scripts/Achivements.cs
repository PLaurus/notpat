﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.EventSystems;

public class Achivements : MonoBehaviour {
	public string leaderboard = "CgkI3cLJyvEWEAIQCg";
	public string achievement1 = "CgkI3cLJyvEWEAIQAQ";//Первопроходец.
	public string achievement2 = "CgkI3cLJyvEWEAIQAg";//Мастер.
	public string achievement3 = "CgkI3cLJyvEWEAIQAw";//Рожденный, чтобы летать.
	public string achievement4 = "CgkI3cLJyvEWEAIQBA";//Перфекционист.
	public string achievement5 = "CgkI3cLJyvEWEAIQBQ";//Сказка.
	public string achievement6 = "CgkI3cLJyvEWEAIQBg";//Все под контролем.
	public string achievement7 = "CgkI3cLJyvEWEAIQBw";//Под контролем профессионала.
	public string achievement8 = "CgkI3cLJyvEWEAIQCA";//Важная информация.
	public string achievement9 = "CgkI3cLJyvEWEAIQCQ";//Наблюдатель.
	public string achievement10 = "CgkI3cLJyvEWEAIQCw";//Достигая высот.
	public string achievement11 = "CgkI3cLJyvEWEAIQDA";//Я легенда

	public GameObject[] buttons = new GameObject[2];
	public Text userNameText;

	void Start () {
		PlayGamesPlatform.Activate();
		Authenticate ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void Authenticate(){
		Social.localUser.Authenticate ((bool success) => {
			if (success)
			{
				userNameText.text = ((PlayGamesLocalUser)Social.localUser).userName;
				foreach(GameObject button in buttons){
					button.SetActive(true);// = enabledColor;
				}
			}
			else
			{
				foreach(GameObject button in buttons){
					button.SetActive(false);
				}
			}	
		});
	}

	public void UnlockAchivement(string achievement){
		if (Social.localUser.authenticated){
			Social.ReportProgress(achievement, 100.0f, (bool success) => {
				if (success)
				{
					Debug.Log("Achievement successfully obtained");
				}
				else
				{
					Debug.Log("Achievement obtaining failed");
				}
			});
		}
	}

	public void IncrementAchievement(string achievement){
		if (Social.localUser.authenticated)
		{
			PlayGamesPlatform.Instance.IncrementAchievement(achievement, 1, (bool success) =>
				{
					if (success)
					{
						Debug.Log("Achievement successfully incremented");
					}
					else
					{
						Debug.Log("Achievement incrementing failed");
					}
				});
		}
	}

	public void IncrementAchievement(string achievement,int step){
		if (Social.localUser.authenticated)
		{
			PlayGamesPlatform.Instance.IncrementAchievement(achievement, step, (bool success) =>
				{
					if (success)
					{
						Debug.Log("Achievement successfully incremented");
					}
					else
					{
						Debug.Log("Achievement incrementing failed");
					}
				});
		}
	}

	public void ReportScore(int score){
		if (Social.localUser.authenticated)
		{
			Social.ReportScore(score, leaderboard, (bool success) =>
				{
					if (success)
					{
						Debug.Log("Score successfully reported");
					}
					else
					{
						Debug.Log("Score report failed");	
					}
				});
		}
	}

	public void ShowLeaderBoard(){
		if (Social.localUser.authenticated) {
			PlayGamesPlatform.Instance.ShowLeaderboardUI ("CgkI3cLJyvEWEAIQCg");
			IncrementAchievement (achievement9);
		} else {
			Authenticate ();
			PlayGamesPlatform.Instance.ShowLeaderboardUI ("CgkI3cLJyvEWEAIQCg");
			IncrementAchievement (achievement9);
		}
	}

	public void ShowAchievements(){
		if (Social.localUser.authenticated) {
			Social.ShowAchievementsUI ();
			IncrementAchievement (achievement10);
		} else {
			Authenticate ();
			PlayGamesPlatform.Instance.ShowLeaderboardUI ("CgkI3cLJyvEWEAIQCg");
			IncrementAchievement (achievement10);
		}
	}

	public void SignOut(){
		((PlayGamesPlatform)Social.Active).SignOut();
	}


}
