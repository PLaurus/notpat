﻿using UnityEngine;
using System.Collections;

public class SoundSwitcher : MonoBehaviour {
	
	public GameObject soundOnIcon;
	public GameObject soundOffIcon;
	public AudioSource[] audioSource = new AudioSource[1];
	public Saver saver;
	private bool sound;

	public void SwitchSoundOn(){
		
		foreach (AudioSource audio in audioSource) {
			audio.mute = false;
		}
		soundOnIcon.SetActive (true);
		soundOffIcon.SetActive (false);
		sound = true;
		saver.Save ();
	}

	public void SwitchSoundOff(){
		
		foreach (AudioSource audio in audioSource) {
			audio.mute = true;
		}
		soundOffIcon.SetActive (true);
		soundOnIcon.SetActive (false);
		sound = false;
		saver.Save ();
	}

	public bool getSound(){
		return sound;
	}


}
