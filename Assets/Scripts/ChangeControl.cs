﻿using UnityEngine;
using System.Collections;

public class ChangeControl : MonoBehaviour {
	//constants
	/*
	private Vector2 lBtnLmin= new Vector2(0f,0f);//
	private Vector2 lBtnLmax= new Vector2(0.147f,1f);//
	private Vector2 lBtnRmin= new Vector2(0.598f,0f);//
	private Vector2 lBtnRmax= new Vector2(0.7450001f,1f);//
	private Vector2 rBtnLmin= new Vector2(0.258f,0f);//
	private Vector2 rBtnLmax= new Vector2(0.4023044f,1f);//
	private Vector2 rBtnRmin= new Vector2(0.8563913f,0f);
	private Vector2 rBtnRmax= new Vector2(1f,1f);
	//----------

	private int controlId = 0;// id способа управления


	[Tooltip("Кнопка игры \"влево\"")]
	public GameObject leftButton;
	[Tooltip("Кнопка игры \"вправо\"")]
	public GameObject rightButton;
	[Tooltip("Кнопка настроек \"влево\"")]
	public GameObject leftSettingsButton;
	[Tooltip("Кнопка настроек \"вправо\"")]
	public GameObject rightSettingstButton;
	[Tooltip("MoveButtons для кнопок окна игры ")]
	public CharacterMove characterMove;
	public CharacterMove3D characterMove3D;
	public Saver saver;

	void Start () {
		
	}

	void Update () {
		

	}

	public void SaveChanges(){
		switch (controlId) {
		case 0:
			leftButton.GetComponent<RectTransform> ().anchorMin = lBtnLmin;
			leftButton.GetComponent<RectTransform> ().anchorMax = lBtnLmax;
			rightButton.GetComponent<RectTransform> ().anchorMin = rBtnLmin;
			rightButton.GetComponent<RectTransform> ().anchorMax = rBtnLmax;
			leftButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			break;
		case 1:
			leftButton.GetComponent<RectTransform> ().anchorMin = lBtnLmin;
			leftButton.GetComponent<RectTransform> ().anchorMax = lBtnLmax;
			rightButton.GetComponent<RectTransform> ().anchorMin = rBtnRmin;
			rightButton.GetComponent<RectTransform> ().anchorMax = rBtnRmax;
			leftButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			break;
		case 2:
			leftButton.GetComponent<RectTransform> ().anchorMin = lBtnRmin;
			leftButton.GetComponent<RectTransform> ().anchorMax = lBtnRmax;
			rightButton.GetComponent<RectTransform> ().anchorMin = rBtnRmin;
			rightButton.GetComponent<RectTransform> ().anchorMax = rBtnRmax;
			leftButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			break;
		}
		saver.Save ();
	}

	public void SaveChanges(int id){
		switch (id) {
		case 0:
			leftButton.GetComponent<RectTransform> ().anchorMin = lBtnLmin;
			leftButton.GetComponent<RectTransform> ().anchorMax = lBtnLmax;
			rightButton.GetComponent<RectTransform> ().anchorMin = rBtnLmin;
			rightButton.GetComponent<RectTransform> ().anchorMax = rBtnLmax;
			leftButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			break;
		case 1:
			leftButton.GetComponent<RectTransform> ().anchorMin = lBtnLmin;
			leftButton.GetComponent<RectTransform> ().anchorMax = lBtnLmax;
			rightButton.GetComponent<RectTransform> ().anchorMin = rBtnRmin;
			rightButton.GetComponent<RectTransform> ().anchorMax = rBtnRmax;
			leftButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			break;
		case 2:
			leftButton.GetComponent<RectTransform> ().anchorMin = lBtnRmin;
			leftButton.GetComponent<RectTransform> ().anchorMax = lBtnRmax;
			rightButton.GetComponent<RectTransform> ().anchorMin = rBtnRmin;
			rightButton.GetComponent<RectTransform> ().anchorMax = rBtnRmax;
			leftButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			break;
		}
	}

	private void TempChanges(int i){
		switch (i) {
		case 0:
			leftSettingsButton.GetComponent<RectTransform> ().anchorMin = lBtnLmin;
			leftSettingsButton.GetComponent<RectTransform> ().anchorMax = lBtnLmax;
			rightSettingstButton.GetComponent<RectTransform> ().anchorMin = rBtnLmin;
			rightSettingstButton.GetComponent<RectTransform> ().anchorMax = rBtnLmax;
			leftSettingsButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			rightSettingstButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			//controlId = i;
			break;
		case 1:
			leftSettingsButton.GetComponent<RectTransform> ().anchorMin = lBtnLmin;
			leftSettingsButton.GetComponent<RectTransform> ().anchorMax = lBtnLmax;
			rightSettingstButton.GetComponent<RectTransform> ().anchorMin = rBtnRmin;
			rightSettingstButton.GetComponent<RectTransform> ().anchorMax = rBtnRmax;
			//leftSettingsButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			//rightSettingstButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			//controlId = i;
			break;
		case 2:
			leftSettingsButton.GetComponent<RectTransform> ().anchorMin = lBtnRmin;
			leftSettingsButton.GetComponent<RectTransform> ().anchorMax = lBtnRmax;
			rightSettingstButton.GetComponent<RectTransform> ().anchorMin = rBtnRmin;
			rightSettingstButton.GetComponent<RectTransform> ().anchorMax = rBtnRmax;
			//leftSettingsButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			//rightSettingstButton.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			//controlId = i;
			break;
		}
	}

	private void ChangeControlWay(int i){
		//moveButtonsSettings.IsMove3D = true;
		//moveButtons.IsMove3D = false;
		switch (i) {
		case 0:
			//characterMove3D.especial = false;
			characterMove.especial = false;
			break;
		case 1:
			//characterMove3D.especial = true;
			characterMove.especial = true;
			break;
		case 2:
			//characterMove3D.especial = false;
			characterMove.especial = false;
			break;
		}
	}

	public void setControlId(int id){
		controlId = id;
		TempChanges (controlId);
		ChangeControlWay (controlId);
	}

	*/
}
