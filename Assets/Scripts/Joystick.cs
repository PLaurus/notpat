﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

//[RequireComponent(typeof(Image))]

public class Joystick : SharedVariables,IDragHandler,IPointerUpHandler,IPointerDownHandler {
	public Image backgroundImage;
	public Image joystickImage;
	public Achivements achievements;
	private Vector2 inputVector;
	public bool visible = false;

	private void Start(){
		//backgroundImage = GetComponent<Image> (); 
	}

	private void Update(){
		if (curSceene == Sceene.PAUSE)
			character.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
	}

	public virtual void OnDrag(PointerEventData ped){
		if (curSceene == Sceene.GAME) {
			Vector2 position;
			if (RectTransformUtility.ScreenPointToLocalPointInRectangle (backgroundImage.rectTransform, ped.position,
				   ped.pressEventCamera, out position)) {
				position.x = 2 * position.x / backgroundImage.rectTransform.rect.width;
				position.y = 2 * position.y / backgroundImage.rectTransform.rect.height;

				inputVector = new Vector2 (position.x, position.y);
				inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;
				joystickImage.rectTransform.localPosition = new Vector2 (inputVector.x * backgroundImage.rectTransform.rect.width / 3,
					inputVector.y * backgroundImage.rectTransform.rect.height / 3); // для точного отображения каcания нужно делить на 2
			
			}
		}
	}

	public virtual void OnPointerDown(PointerEventData ped){
		if (curSceene == Sceene.GAME) {
			if (!visible) {
				backgroundImage.rectTransform.position = Camera.main.ScreenToWorldPoint (ped.position);
				//Debug.Log (Camera.main.ScreenToWorldPoint (ped.position));
				//joystickImage.rectTransform.position = ped.position;
				backgroundImage.enabled = true;
				joystickImage.enabled = true;
				visible = true;
				achievements.IncrementAchievement (achievements.achievement6);
				achievements.IncrementAchievement (achievements.achievement7);
			}
		}
	}

	public virtual void OnPointerUp(PointerEventData ped){
		backgroundImage.enabled = false;
		joystickImage.enabled = false;
		visible = false;
		//Debug.Log (joystickImage.enabled);
		inputVector = Vector2.zero;
		joystickImage.rectTransform.localPosition = Vector2.zero;
	}

	public float Horizontal(){
		return inputVector.x;
	}

	public float Vertical(){
		return inputVector.y;
	}
		
	public void MakeVisible(Vector2 pos){
		if (!visible) {
			backgroundImage.rectTransform.position = Camera.main.ScreenToWorldPoint (pos);
			backgroundImage.enabled = true;
			joystickImage.enabled = true;
			visible = true;
			Debug.Log (joystickImage.enabled);
		}
	}
}
