﻿using UnityEngine;
using System.Collections;

public class ObstaclesSpawner : SharedVariables {
	private const int CLONE_COUNT = 3;

	public GameObject[] twoStartObstacles = new GameObject[2];
	public GameObject[] obstacles = new GameObject[4];
	public GameObject lastObstacle;

	private GameObject fixedLastObstacle;
	private GameObject[,] allObstacles;
	private float S;
	private float screenRightBorderPos;
	private float obstacleRightBorderPos;
	private bool obstaclesReadyToSpawn = false;
	private int newObstacleId;
	private Vector3 pos;
	private Vector3 disabledObjsPos;

	void Start () {
		fixedLastObstacle = lastObstacle;
		allObstacles = new GameObject[obstacles.Length,CLONE_COUNT];
		disabledObjsPos = Camera.main.ScreenToWorldPoint (new Vector3 (-2 * Screen.width, Screen.height / 2));
		for (int i = 0; i < obstacles.Length; i++) {
			for (int j = 0; j < CLONE_COUNT; j++) {
				allObstacles [i,j] = Instantiate (obstacles [i], disabledObjsPos, Quaternion.identity,transform) as GameObject;
				allObstacles [i, j].GetComponent<ObjsActivitySwitcher> ().setIds (i, j);
			}
		}
		setS ();
		obstacleRightBorderPos = lastObstacle.GetComponent<SpriteRenderer>().transform.position.x +
			lastObstacle.GetComponent<SpriteRenderer>().bounds.extents.x;
	}

	void Update(){
		if (curSceene == Sceene.GAME) {
			screenRightBorderPos = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x;
			GenerateNewId ();
			if ((screenRightBorderPos - obstacleRightBorderPos) > 0) {
				float NextObstacleHalfScale = obstacles [newObstacleId].GetComponent<SpriteRenderer> ().bounds.extents.x;
				pos = new Vector3 (obstacleRightBorderPos + S + NextObstacleHalfScale, obstacles [newObstacleId].transform.position.y,
					GameObject.FindGameObjectWithTag ("Background").transform.position.z / 2);
				spawn ();
			}
		}
	}

	void GenerateNewId(){
		if (!obstaclesReadyToSpawn) {
			newObstacleId = Random.Range (0, obstacles.Length);
			obstaclesReadyToSpawn = true;
		}
	}

	void GenerateNewId(int exceptThisId){
		if (exceptThisId >= 1) {
			newObstacleId--;
		} else if (exceptThisId == 0) {
			newObstacleId++;
		}
	}

	void spawn(){
		for (int i = 0; i < CLONE_COUNT; i++) {
			if (!allObstacles [newObstacleId, i].activeInHierarchy) {
				allObstacles [newObstacleId, i].transform.position = pos;
				allObstacles [newObstacleId, i].GetComponent<ObjsActivitySwitcher> ().setRightBorderPos ();

				foreach (Transform go in allObstacles [newObstacleId, i].GetComponentsInChildren<Transform>(true)) {
					if (go.gameObject.CompareTag ("Coin"))
						go.gameObject.SetActive (true);
				}

				allObstacles [newObstacleId, i].SetActive (true);
				lastObstacle = allObstacles [newObstacleId, i];
				obstacleRightBorderPos = lastObstacle.GetComponent<SpriteRenderer> ().transform.position.x +
				lastObstacle.GetComponent<SpriteRenderer> ().bounds.extents.x;
				obstaclesReadyToSpawn = false;
				break;
			}
			if (i == (CLONE_COUNT - 1)) {
				GenerateNewId (newObstacleId);
				spawn ();
			}
		}
	}

	void setS(){
		S = Mathf.Abs(twoStartObstacles [1].GetComponent<SpriteRenderer>().transform.position.x - twoStartObstacles [0].GetComponent<SpriteRenderer>().transform.position.x 
			- twoStartObstacles [0].GetComponent<SpriteRenderer>().bounds.extents.x - twoStartObstacles [1].transform.GetComponent<SpriteRenderer>().bounds.extents.x);
	}

	public float GetS(){
		return S;
	}

	public Vector3 getDisabledObjsPos(){
		return disabledObjsPos;
	}

	public void disablePlat(int i,int j){
		allObstacles [i, j].SetActive (false);
	}

	public void Reset(){
		for (int i = 0; i < obstacles.Length; i++) {
			for (int j = 0; j < CLONE_COUNT; j++) {
				allObstacles [i,j].SetActive(false);
				allObstacles [i, j].transform.position = disabledObjsPos;
			}
		}
		lastObstacle = fixedLastObstacle;
		obstacleRightBorderPos = lastObstacle.GetComponent<SpriteRenderer>().transform.position.x +
			lastObstacle.GetComponent<SpriteRenderer>().bounds.extents.x;
	}
}

