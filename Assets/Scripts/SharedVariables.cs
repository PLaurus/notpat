﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SharedVariables : MonoBehaviour
{
	protected static Transform GAME_ENVIRORMENT_SV;
	protected static Transform character;
	protected static CharacterMove characterMove;
	protected static ObstaclesSpawner obstaclesSpawner;
	private static Vector3 startCharacterPos;

	protected enum Sceene
	{
		MENU,
		GAME,
		RESULT_WINDOW,
		SETTINGS,
		EXIT,
		PAUSE,
	}

	protected static uint curScore;
	protected static uint highScore = 0;
	protected static Sceene curSceene;

	protected static uint coinsCount = 0;
	protected static Bird[] birds = new Bird[8];

	void Start ()
	{
		ResetScore ();
		curSceene = Sceene.MENU;
		GAME_ENVIRORMENT_SV = GameObject.FindGameObjectWithTag ("GameEnvirorment").transform;
		obstaclesSpawner = GAME_ENVIRORMENT_SV.gameObject.GetComponent<ObstaclesSpawner> ();
		character = GameObject.FindGameObjectWithTag ("Player").transform;
		characterMove = character.GetComponent<CharacterMove> ();
		startCharacterPos = character.transform.position;
		Saver saver = GameObject.FindGameObjectWithTag ("GameEnvirorment").GetComponent<Saver> ();
		saver.Load ();
	}

	void Update ()
	{
		SetCharacter ();
	}

	protected static void SetCharacter ()
	{
		if (character == null && curSceene == Sceene.MENU) {
			character = GameObject.Find ("Character").transform;
		}
	}

	protected static void ResetScore ()
	{
		curScore = 0;
	}

	protected static void ResetCharacterPos(){
		character.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 0f);
		character.position = startCharacterPos;
	}
}
