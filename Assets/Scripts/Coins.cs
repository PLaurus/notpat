﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Coins : SharedVariables {

	public Text CoinsText;
	public AudioSource audioSource;
	private uint multiplier = 1;
	void Start () {
		foreach (Bird bird in birds) {
			if (bird.selected) {
				if (bird.cost == 4000) {
					multiplier = 2;
					break;
				} else if (bird.cost == 9000) {
					multiplier = 3;
					break;
				} else if (bird.cost == 30000) {
					multiplier = 4;
					break;
				} else if (bird.cost == 70000) {
					multiplier = 5;
					break;
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Coin") {
			other.gameObject.SetActive (false);
			coinsCount += 1*multiplier;
			CoinsText.text = coinsCount.ToString ();
			audioSource.Play ();
		}
	}

	public void ChangeMultiplier(){
		Start ();
	}
}
