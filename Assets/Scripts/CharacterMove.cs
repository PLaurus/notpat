﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
[RequireComponent (typeof(BoxCollider2D))]
[RequireComponent (typeof(Rigidbody2D))]
public class CharacterMove : SharedVariables
{
	//CONSTANTS
	private const float START_SPEED = 192.0f;
	//private
	private float speed = 193.0f;//300
	private bool facingRight = true;
	private Vector3 theScale;
	private bool xAxisActive = false;
	private bool yAxisActive = false;
	private bool joystickCtrl = true,keyboardCtrl = false;
	private float constSpeedUp = 0f;

	//public
	[Tooltip("Когда зажата одна кнопка, другая становится прыжком")]
	//public bool especial = false;
	//public bool activeCharacter = false;
	public Achivements achievements;
	public GameObject leftButton, rightButton, jumpButton;
	public Joystick joystick;
	public TornadoMoving tornadoMoving;

	public void IncreaseSpeed(){
		speed += constSpeedUp;
	}

	public void ResetSpeed(){
		speed = START_SPEED;
	}

	void Start(){
		//float govno = tornadoMoving.GetFinalSpeed ();
		//float govno2 = tornadoMoving.GetStartSpeed ();
		constSpeedUp = (((tornadoMoving.GetFinalSpeed() * START_SPEED)/tornadoMoving.GetStartSpeed())-START_SPEED) / 6.0f;
		//Debug.Log ("constSpeedUp: "+constSpeedUp);
		theScale = transform.localScale;
		//Input.gyro.enabled = true; //UseAccelerator;
	}

	void FixedUpdate(){
		if (curSceene == Sceene.GAME) {
			if (keyboardCtrl) {
				if (Input.GetKey (KeyCode.A)) {
					goleft ();
				} else if (Input.GetKey (KeyCode.D))
					goRight ();
				else
					xAxisActive = false;
				if (Input.GetKey (KeyCode.W)) {
					goUp ();
				} else if (Input.GetKey (KeyCode.S))
					goDown ();
				else
					yAxisActive = false;
			} else if (joystickCtrl) {
				if (joystick.Horizontal () < 0) {
					goleft (joystick.Horizontal ());
				} else if (joystick.Horizontal () > 0) {
					goRight (joystick.Horizontal ());
				} else
					xAxisActive = false;
				if (joystick.Vertical () > 0) {
					goUp (joystick.Vertical ());
				} else if (joystick.Vertical () < 0) {
					goDown(joystick.Vertical ());
				} else
					yAxisActive = false;
			}
			SetCommonValue ();
		}
	}

	private void goleft ()
	{
		xAxisActive = true;
		if (facingRight) rotateCharacter ();
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (-Time.deltaTime * speed,
			GetComponent<Rigidbody2D> ().velocity.y);
	}

	private void goleft (float sensitivity)
	{
		xAxisActive = true;
		if (facingRight) rotateCharacter ();
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (Time.deltaTime * speed * sensitivity,
			GetComponent<Rigidbody2D> ().velocity.y);
	}

	private void goRight ()
	{
		xAxisActive = true;
		if (!facingRight) rotateCharacter ();
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (Time.deltaTime * speed,
			GetComponent<Rigidbody2D> ().velocity.y);
	}

	private void goRight (float sensitivity)
	{
		xAxisActive = true;
		if (!facingRight) rotateCharacter ();
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (Time.deltaTime * speed * sensitivity,
			GetComponent<Rigidbody2D> ().velocity.y);
	}

	private void goUp ()
	{
		yAxisActive = true;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x,
			Time.deltaTime * speed);
	}

	private void goUp (float sensitivity)
	{
		yAxisActive = true;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x,
			Time.deltaTime * speed * sensitivity);
	}

	private void goDown ()
	{
		yAxisActive = true;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x,
			-Time.deltaTime * speed);
	}

	private void goDown (float sensitivity)
	{
		yAxisActive = true;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x,
			Time.deltaTime * speed * sensitivity);
	}

	void rotateCharacter ()
	{
		facingRight = !facingRight;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void SetCommonValue ()
	{
		if (!xAxisActive && !yAxisActive)
			GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		else {
			if(!xAxisActive) GetComponent<Rigidbody2D> ().velocity = new Vector2(0f,GetComponent<Rigidbody2D> ().velocity.y);//GetComponent<Rigidbody2D> ().velocity.y);
			if(!yAxisActive) GetComponent<Rigidbody2D> ().velocity = new Vector2(GetComponent<Rigidbody2D> ().velocity.x,0f);
		}
	}
}
