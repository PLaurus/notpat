﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraPosController : CheckPoint {
	private Vector3 startPos;
	private const float SPEED1 = 0.005f,SPEED2 = 0.001f;

	//public GameObject hills1, hills2;
	public EndlessHills endlesshills;

	void Start ()
	{
		startPos = transform.position;
	}

	void Update() {
		if (curSceene == Sceene.GAME) {
			if (changedRight && character.GetComponent<Rigidbody2D> ().velocity.x > 0f) {
				transform.position += new Vector3 (character.GetComponent<Rigidbody2D> ().velocity.x * Time.deltaTime, 0f, 0f);
				foreach(SpriteRenderer sr in endlesshills.hills1) sr.transform.position -= new Vector3 (SPEED1, 0f, 0f);
				foreach(SpriteRenderer sr in endlesshills.hills2) sr.transform.position -= new Vector3 (SPEED2, 0f, 0f);
				ResetCheckPoints ();
			} else if (changedLeft && character.GetComponent<Rigidbody2D> ().velocity.x < 0f) {
				transform.position += new Vector3 (character.GetComponent<Rigidbody2D> ().velocity.x * Time.deltaTime, 0f, 0f);
				foreach(SpriteRenderer sr in endlesshills.hills1) sr.transform.position += new Vector3 (SPEED1, 0f, 0f);
				foreach(SpriteRenderer sr in endlesshills.hills2) sr.transform.position += new Vector3 (SPEED2, 0f, 0f);
				ResetCheckPoints ();
			}else if (changedRight || changedLeft) {
				ResetBools ();
			}
		}
	}

	public void resetPos(){
		transform.position = startPos;
	}
}

