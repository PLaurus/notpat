﻿using UnityEngine;
using System.Collections;
using System;

public class CheckPoint : SharedVariables
{
	public Score score;
	private static Vector3 rightCheckPoint;
	private static Vector3 leftCheckPoint;
	protected static bool changedRight = false;
	protected static bool changedLeft = false;

	void Awake() {
		ResetCheckPoints ();
	}

	void Update () {
		if (curSceene == Sceene.GAME) {
			if (character.position.x > rightCheckPoint.x) {
				changedRight = true;
				score.UpdateScore();
			} else if (character.position.x < leftCheckPoint.x) {
				changedLeft = true;
			}
		}
	}

	public static void ResetCheckPoints(){
		setCheckPoints ();
		ResetBools ();
	}
	public static void ResetBools(){
		changedRight = false;
		changedLeft = false;
	}

	public static void setCheckPoints(){
		rightCheckPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height/2, 0f));
		leftCheckPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 1 / 4, Screen.height/2, 0f));
	}
}

