﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Saver : SharedVariables {
	//public TabsSwitcher tabsSwitcher;
	//public AudioSource audioSource;
	public SoundSwitcher soundSwitcher;
	//public ChangeControl changeControl;
	public Text highScoreText, highScoreTextInfo;
	public Text coinsText;
	public ChangeBird changeBird;
	public Bonus bonus;
	private string path;
	//private SaveManager saveManager;

	public Saver () {
		
	}

	public void Save () {
		path = Application.persistentDataPath + "/NewSaveFile1.wr";
		BinaryFormatter binary = new BinaryFormatter ();
		FileStream fStream = File.Create (path);
		SaveManager.GetInstance ().SetValues (highScore, soundSwitcher.getSound (),coinsCount,birds,
			bonus.previousDay,bonus.todayCoinsBonus);
		binary.Serialize (fStream, SaveManager.GetInstance());
		fStream.Close ();
	}

	public void Load () {
		path = Application.persistentDataPath + "/NewSaveFile1.wr";
		Debug.Log (path);
		if (File.Exists (path)) {
			BinaryFormatter binary = new BinaryFormatter ();
			FileStream fStream = File.Open (path, FileMode.Open);
			SaveManager.RecoverInstance ((SaveManager)binary.Deserialize (fStream));
			fStream.Close ();
			highScore = SaveManager.GetInstance ().sHighScore;
			highScoreText.text = highScoreTextInfo.text = SaveManager.GetInstance ().sHighScore.ToString ();
			coinsCount = SaveManager.GetInstance ().sCoins;
			coinsText.text = SaveManager.GetInstance ().sCoins.ToString ();
			birds = SaveManager.GetInstance ().getBirds ();
			changeBird.secondLaunch ();
			bonus.setPreviousDay(SaveManager.GetInstance ().rewardPreviousDay);
			bonus.setCoinsBonus(SaveManager.GetInstance ().todayBonusCoins);
			if (SaveManager.GetInstance ().sound)
				soundSwitcher.SwitchSoundOn ();
			else
				soundSwitcher.SwitchSoundOff ();
		} else {
			coinsCount = 1000;
			coinsText.text = coinsCount.ToString ();
			changeBird.firstLaunch ();
			bonus.setPreviousDay(new DateTime (2017, 08, 17));
		}
	}
}

[Serializable]
class SaveManager{
	private static SaveManager saveManager;

	public DateTime rewardPreviousDay{ get; private set;}
	public uint todayBonusCoins{ get; private set;}
	public uint sHighScore{ get; private set;}
	public bool sound{ get; private set;}
	public uint sCoins{ get; private set;}
	private Bird[] birds;

	private SaveManager(){
		rewardPreviousDay = new DateTime (2017, 08, 17);
		todayBonusCoins = 0;
		sCoins = 150;
		sHighScore = 0;
		sound = true;
		birds = new Bird[8];
	}

	public static SaveManager GetInstance(){
		if (saveManager == null) {
			saveManager = new SaveManager ();
		}
		return saveManager;
	}

	public static void RecoverInstance(SaveManager serializedInstanse){
		if (saveManager != null) {
			return;
		}
		saveManager = serializedInstanse;
	}

	public void SetValues(uint sHighScore,bool sound,uint sCoins,Bird[] birds,
		DateTime rewardPreviousDay,uint todayBonusCoins){

		this.sHighScore = sHighScore;
		this.sound = sound;
		this.sCoins = sCoins;
		this.birds = birds;
		this.rewardPreviousDay = rewardPreviousDay;
		this.todayBonusCoins = todayBonusCoins;

	}

	public Bird[] getBirds(){
		return birds;
	}
}