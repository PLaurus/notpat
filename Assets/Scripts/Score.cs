﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : SharedVariables {
	public Text scoreNumber;
	private float lastPos;

	void Start () {
		lastPos = 0f;
	}

	void Update(){
		//scoreNumber.text = curScore.ToString();
	}

	public void UpdateScore () {
		if (character.position.x > lastPos && character.position.x > 0f) {
			lastPos = character.position.x;
			curScore = (uint)character.position.x;
			scoreNumber.text = curScore.ToString ();
		}
	}

	public void Reset(){
		lastPos = 0f;
		ResetScore ();
		scoreNumber.text = "0";
	}
}

/*//Использование паттерна Proxy
interface IScore{
	void UpdateScore ();
}

class ObstaclesProxy : IScore{
	
	Score realScore;
	public override void UpdateScore()
	{
		
		if (realScore == null)
			realScore = new Score();
		typeof(a);
		realScore.UpdateScore();
	}
}
//----------------*/
