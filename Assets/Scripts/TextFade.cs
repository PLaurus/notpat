﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextFade : MonoBehaviour {

	private Text txt;

	void Start () {
		txt = GetComponent<Text> ();
	}

	void Update () {
		txt.color = new Color (txt.color.r, txt.color.g, txt.color.b, Mathf.PingPong (Time.time/2, 1f));
	}
}
