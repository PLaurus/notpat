﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
[RequireComponent (typeof(BoxCollider))]
[RequireComponent (typeof(Rigidbody))]
public class CharacterMove3D : SharedVariables
{
	//CONSTANTS
	private const string LEFT_TXT = "LEFT", RIGHT_TXT = "RIGHT", JUMP_TXT = " JUMP";

	//private
	private float speed = 150f, jump = 800f;
	private bool grounded = false;
	private bool facingRight = true, rightPressed = false, leftPressed = false;
	private Vector3 theScale;//Используется?
	private bool buttonsActivity = false;

	//public
	[Tooltip("Когда зажата одна кнопка, другая становится прыжком")]
	public bool especial = false;
	public bool activeCharacter = false;
	public Achivements achievements;
	public GameObject leftButton, rightButton, jumpButton;

	void Start ()
	{
		theScale = transform.localScale;//Используется?
		GetComponent<Rigidbody> ().velocity = Vector2.zero;
	}

	void FixedUpdate(){
		if ((curSceene == Sceene.GAME || curSceene == Sceene.SETTINGS) && activeCharacter) {
			if (!especial) {
				foreach (Touch touch in Input.touches) {
					Ray ray = Camera.main.ScreenPointToRay (touch.position);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit)) {
						if (hit.transform.gameObject == leftButton)
							goleft ();
						else if (hit.transform.gameObject == rightButton)
							goRight ();
						else
							SetCommonValue ();
						if (hit.transform.gameObject == jumpButton)
							jmp ();
					}
				}
			} else {
				foreach (Touch touch in Input.touches) {
					Ray ray = Camera.main.ScreenPointToRay (touch.position);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit)) {
						if (hit.transform.gameObject == leftButton) {
							if (leftButton.GetComponentInChildren<Text>().text == LEFT_TXT) {
								rightButton.GetComponentInChildren<Text>().text = JUMP_TXT;
								goleft ();
							} else if (leftButton.GetComponentInChildren<Text>().text == JUMP_TXT) {
								jmp ();
							}
						} else if (rightButton.GetComponentInChildren<Text>().text == JUMP_TXT) {
							rightButton.GetComponentInChildren<Text>().text = RIGHT_TXT;
							leftButton.GetComponentInChildren<Text>().text = LEFT_TXT;
							character.GetComponent<CharacterMove> ().SetCommonValue();//3d
						}
						if (hit.transform.gameObject == rightButton) {
							if (rightButton.GetComponentInChildren<Text>().text == RIGHT_TXT) {
								leftButton.GetComponentInChildren<Text>().text = JUMP_TXT;
								goRight ();
							} else if (rightButton.GetComponentInChildren<Text>().text == JUMP_TXT) {
								jmp ();
							}
						} else if (leftButton.GetComponentInChildren<Text>().text == JUMP_TXT) {
							rightButton.GetComponentInChildren<Text>().text = RIGHT_TXT;
							leftButton.GetComponentInChildren<Text>().text = LEFT_TXT;
							character.GetComponent<CharacterMove> ().SetCommonValue();//3d
						}
						if (hit.transform.gameObject == jumpButton) {
							jmp ();
						}
					}
				}
			}
		}
	}

	void OnTriggerEnter2D ()
	{
		GetComponent<Rigidbody> ().velocity = new Vector2 (0f, 0f);// Dont delete!!!
	}

	void OnTriggerStay2D ()
	{
		grounded = true;
	}

	void OnTriggerExit2D ()
	{
		grounded = false;
	}

	void rotateCharacter ()
	{
		facingRight = !facingRight;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void goleft ()
	{
		if (!rightPressed) {
			leftPressed = true;
			if (facingRight) rotateCharacter ();
			GetComponent<Rigidbody> ().velocity = new Vector2 (-Time.deltaTime * speed,
				GetComponent<Rigidbody> ().velocity.y);
		}
	}

	public void goRight ()
	{
		if (!leftPressed) {
			rightPressed = true;
			if (!facingRight) rotateCharacter ();
			GetComponent<Rigidbody> ().velocity = new Vector2 (Time.deltaTime * speed,
				GetComponent<Rigidbody> ().velocity.y);
		}
	}

	public void jmp ()
	{
		if (grounded) {
			//GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 0f);
			GetComponent<Rigidbody> ().AddForce (new Vector2 (0f, jump));
			GetComponent<Rigidbody> ().velocity = new Vector2 (0f, 0f);
			achievements.IncrementAchievement (achievements.achievement6);
			achievements.IncrementAchievement (achievements.achievement7);
		}
	}

	public void SetCommonValue ()
	{
		leftPressed = false;
		rightPressed = false;
		if (grounded) {
			GetComponent<Rigidbody> ().velocity = new Vector2 (0f, GetComponent<Rigidbody> ().velocity.y);
		}
	}

	public void SwitchActivity ()
	{
		buttonsActivity = !buttonsActivity;
		if (buttonsActivity) {
			leftButton.SetActive (true);
			leftButton.GetComponent<ButtonAnimation> ().PointerUp ();//
			rightButton.SetActive (true);
			rightButton.GetComponent<ButtonAnimation> ().PointerUp ();//
		} else {
			leftButton.SetActive (false);
			rightButton.SetActive (false);
		}
	}

	public void ResetButtons ()
	{
		leftButton.GetComponentInChildren<Text>().text = LEFT_TXT;
		rightButton.GetComponentInChildren<Text>().text = RIGHT_TXT;
	}

}