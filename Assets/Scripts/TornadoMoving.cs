﻿using UnityEngine;
using System.Collections;

public class TornadoMoving : SharedVariables {
	private const float START_SPEED = 3.6f;//3,3
	private const float FINAL_SPEED = 6.5f;
	private const uint CONST_UP = 2;//multiplier
	private float ADDITIONAL_SPEED = 0.53f;
	private const uint nextSU = 5;
	private uint nextSpeedUp = 5;//score
	private float speed;
	private Vector3 startPos;

	//private const float HARD_SPEED = 5.6f;
	//private const uint HARD_SCORE = 1000;

	//public CharacterMove characterMove;

	void Start () {
		ADDITIONAL_SPEED = (FINAL_SPEED - START_SPEED) / 6.0f;
		startPos = transform.position;
		speed = START_SPEED;
		//float z = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Transform> ().position.y;
		//float a = Camera.main.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2, 0f)).y;
		//transform.position = new Vector3(0f,3 * Camera.main.ScreenToWorldPoint (Vector3.zero).y/2 -
			//Camera.main.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y/2,0f);
	}

	void Update () {
		//Debug.Log (curSceene);///////
		if (curSceene == Sceene.GAME) {
			ChangeSpeed ();
			transform.position += new Vector3 (speed*Time.deltaTime, 0f);
		}
	}

	public float GetStartSpeed(){
		return START_SPEED;
	}

	public float GetFinalSpeed(){
		return FINAL_SPEED;
	}

	void ChangeSpeed(){
		if (speed < FINAL_SPEED) {
			if (curScore >= nextSpeedUp) {
				nextSpeedUp *= CONST_UP;
				speed += ADDITIONAL_SPEED;
				characterMove.IncreaseSpeed ();
			}
		} /*else if((speed != HARD_SPEED) && (curScore > HARD_SCORE)){
			speed = HARD_SPEED;
			Vector3 temp =  new Vector3(3 * Camera.main.ScreenToWorldPoint (Vector3.zero).x/2 -
				Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x/2,transform.position.y,
				transform.position.z);
			if (temp.x > transform.position.x)
				transform.position = temp;
		}*/
	}

	public void Reset(){
		transform.position = startPos;
		speed = START_SPEED;
		nextSpeedUp = nextSU;
		characterMove.ResetSpeed ();
	}
}
