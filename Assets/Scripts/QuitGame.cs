﻿using UnityEngine;
using System.Collections;

public class QuitGame : SharedVariables {
	public GameObject ExitCanvas;
	public GameObject[] gameObjsToDisable = new GameObject[3];
	public Saver saver;
	private Sceene previousSceene = Sceene.MENU;

	void Start () {
	
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && ((curSceene == Sceene.MENU)||(curSceene == Sceene.PAUSE)))
			LoadExitWindow ();
		//Quit ();
	}

	public void Quit(){
			saver.Save ();
			Application.Quit(); 
	}

	public void LoadExitWindow () {
		previousSceene = curSceene;
		ExitCanvas.SetActive (true);
		if (previousSceene != Sceene.PAUSE) {
			foreach (GameObject go in gameObjsToDisable)
				go.SetActive (false);
		}
		curSceene = Sceene.EXIT;
	}

	public void CloseExitWindow () {
		if (previousSceene != Sceene.PAUSE) {
			foreach (GameObject go in gameObjsToDisable)
				go.SetActive (true);
		}
		curSceene = previousSceene;
		ExitCanvas.SetActive(false);
	}


}
